package com.flaremicrosystems.temp;

import java.awt.EventQueue;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import com.flaremicrosystems.temp.ui.MainFrame;
import com.flaremicrosystems.util.DPIUtil;

public class StarterClass {
	public static void main(String[] args) {
		PropertiesManager.init();
		PropertiesManager.flush();
		try
		{
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
			{
				if ("Nimbus".equals(info.getName()))
				{
					UIManager.setLookAndFeel(info.getClassName());
				}
			}
		}
		catch (Exception e){}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try
				{
					DPIUtil.setNewScale(PropertiesManager.scaling);
					DPIUtil.setDefaultUIFontsToScale();

					MainFrame frame = new MainFrame();
					frame.setFullScreen(PropertiesManager.fullScreen);
					//frame.setVisible(true);
				}
				catch (Throwable e)
				{
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Program has crashed! Try deleting the configuration directory.");
				}
			}
		});
	}
}
