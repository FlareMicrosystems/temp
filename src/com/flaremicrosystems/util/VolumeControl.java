package com.flaremicrosystems.util;

import java.io.File;
import java.io.IOException;

public class VolumeControl {
	
	public static void changeVol(int amount)
	{
		try
		{
			Runtime.getRuntime().exec(new String[]{new File(Util.getBinDirectory(), "nircmd.exe").toString(), "changesysvolume", String.valueOf(amount)});
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
