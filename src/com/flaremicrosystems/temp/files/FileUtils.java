package com.flaremicrosystems.temp.files;

import java.io.File;
import java.util.TreeSet;

public class FileUtils {
	public static TreeSet<Album> getAlbumsInDir(File file)
	{
		TreeSet<Album> albums = new TreeSet<Album>();
		if(file.exists())
		{
			Album unsortedAlbum = Album.fromDir(file, "Unsorted Music");
			for(File album : file.listFiles())
			{
				Album albumInfo = Album.fromDir(album);
				if(albumInfo != null && albumInfo.songs.size() > 0)
					albums.add(albumInfo);
			}
			if(unsortedAlbum.songs.size() > 0)
				albums.add(unsortedAlbum);
		}
		return albums;
	}
}
