package com.flaremicrosystems.temp.ui.dialog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.border.EmptyBorder;
import javax.swing.JSplitPane;

import java.awt.GridLayout;

import javax.swing.JLabel;

import com.flaremicrosystems.temp.PropertiesManager;
import com.flaremicrosystems.temp.ui.FileSelector;
import com.flaremicrosystems.temp.ui.MainFrame;
import com.flaremicrosystems.temp.ui.layout.WrapLayout;
import com.flaremicrosystems.util.DPIUtil;

import javax.swing.JCheckBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.SpinnerNumberModel;

public class ConfigurationDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private FileSelector fsMusicDir;
	private JSpinner spnScaling;
	JCheckBox chckbxRemoveNames = new JCheckBox("Remove Names (Before First '-')");
	JCheckBox chckbxFullscreen = new JCheckBox("Always Full Screen");
	JSpinner spnBuffer = new JSpinner();
	JSpinner spnNavTime = new JSpinner();
	JCheckBox chckbxLoopPlaylist = new JCheckBox("Loop Playlist");
	MainFrame mainFrame;

	/**
	 * Create the dialog.
	 */
	public ConfigurationDialog(final MainFrame parent) {
		super(parent);
		setTitle("Configuration and Utility Functions");
		setBounds(100, 100, 576, 336);
		DPIUtil.scaleComponent(this);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		contentPanel.setLayout(new BorderLayout(0, 0));

		JSplitPane splitPane = new JSplitPane();
		splitPane.setEnabled(false);
		contentPanel.add(splitPane, BorderLayout.NORTH);

		JPanel panelLeft = new JPanel();
		splitPane.setLeftComponent(panelLeft);
		panelLeft.setLayout(new GridLayout(0, 1, 0, 5));

		JLabel lblUgly = new JLabel("Music Directory");
		panelLeft.add(lblUgly);

		JLabel lblScaling = new JLabel("Scaling");
		panelLeft.add(lblScaling);
		
		JLabel lblBuffer = new JLabel("Buffer");
		panelLeft.add(lblBuffer);

		JLabel lblDescaling = new JLabel("Name Trimming");
		panelLeft.add(lblDescaling);

		JLabel lblRescaling = new JLabel("Full Screen");
		panelLeft.add(lblRescaling);
		
		JLabel lblLoop = new JLabel("Loop Playlist");
		panelLeft.add(lblLoop);
		
		JLabel navThreshold = new JLabel("Takeover Time");
		panelLeft.add(navThreshold);

		JPanel panelRight = new JPanel();
		splitPane.setRightComponent(panelRight);
		panelRight.setLayout(new GridLayout(0, 1, 0, 5));

		fsMusicDir = new FileSelector(PropertiesManager.musicDirectory);
		panelRight.add(fsMusicDir);

		spnScaling = new JSpinner();
		spnScaling.setModel(new SpinnerNumberModel(new Float(PropertiesManager.scaling), new Float(0.5F), new Float(50F), new Float(0.2F)));
		panelRight.add(spnScaling);
		

		spnBuffer.setModel(new SpinnerNumberModel(PropertiesManager.frames, 1, 2048, 1));
		panelRight.add(spnBuffer);

		chckbxRemoveNames.setSelected(PropertiesManager.trimNames);
		panelRight.add(chckbxRemoveNames);

		chckbxFullscreen.setSelected(PropertiesManager.fullScreen);
		panelRight.add(chckbxFullscreen);
		
		chckbxLoopPlaylist.setSelected(PropertiesManager.loopPlaylist);
		panelRight.add(chckbxLoopPlaylist);
		
		spnNavTime.setModel(new SpinnerNumberModel(new Long(0), new Long(0), null, new Long(1)));
		panelRight.add(spnNavTime);

		JPanel panelMiddle = new JPanel();
		panelMiddle.setLayout(new WrapLayout());
		contentPanel.add(panelMiddle, BorderLayout.CENTER);

		JButton btnRefreshMusic = new JButton("Refresh Music");
		btnRefreshMusic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reloadMusic(parent);
			}
		});
		panelMiddle.add(btnRefreshMusic);

		JButton btnClearPlaylist = new JButton("Clear Playlist");
		btnClearPlaylist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parent.clearPlaylist();
			}
		});
		panelMiddle.add(btnClearPlaylist);

		JButton btnReloadProperties = new JButton("Reload Properties");
		btnReloadProperties.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PropertiesManager.init();
			}
		});
		panelMiddle.add(btnReloadProperties);
		
		JButton btnExitToWindows = new JButton("Close Program");
		btnExitToWindows.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		panelMiddle.add(btnExitToWindows);
		
				JLabel lblNewLabel = new JLabel("<html>Changes to 'Scaling' require a restart of the program to be applied.<br>\r\nChanges to 'Music Directory' and 'Name Trimming' will automatically refresh music.</html>");
				contentPanel.add(lblNewLabel, BorderLayout.SOUTH);

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);

		JButton okButton = new JButton("Apply");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean shutdown = false;
				if(chckbxRemoveNames.isSelected() != PropertiesManager.trimNames || PropertiesManager.musicDirectory != fsMusicDir.getFile())
				{
					PropertiesManager.trimNames = chckbxRemoveNames.isSelected();
					PropertiesManager.musicDirectory = fsMusicDir.getFile();
					reloadMusic(parent);
				}
				if(PropertiesManager.scaling != (Float)spnScaling.getValue())
					shutdown = true;
				PropertiesManager.scaling = ((Float)spnScaling.getValue());

				if(chckbxFullscreen.isSelected() != PropertiesManager.fullScreen)
					parent.setFullScreen(chckbxFullscreen.isSelected());
				PropertiesManager.frames = (Integer)spnBuffer.getValue();
				PropertiesManager.navThreshold = (Long)spnNavTime.getValue();
				PropertiesManager.fullScreen = chckbxFullscreen.isSelected();
				PropertiesManager.loopPlaylist = chckbxLoopPlaylist.isSelected();
				PropertiesManager.storeProperties();
				dispose();
				if(shutdown)
				{
					try
					{
						Runtime.getRuntime().exec(new String[]{"java", "-jar", "TEMP.jar"});
					}
					catch (IOException e1)
					{
						e1.printStackTrace();
					}
					System.exit(0);
				}
			}
		});
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);

		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
		setModal(true);
		setLocationRelativeTo(parent);
	}

	private void reloadMusic(MainFrame parent) {
		parent.loadMusic();
	}
	
}
