package com.flaremicrosystems.util;

import java.math.BigInteger;

public class NumberUtil {
	
	public static BigInteger parseBigInt(String string, BigInteger fallback)
	{
		try
		{
			return new BigInteger(string);
		}
		catch(NumberFormatException ex)
		{
			return fallback;
		}
	}
	
	public static long parseLong(String string, long fallback)
	{
		try
		{
			return Long.parseLong(string);
		}
		catch(NumberFormatException ex)
		{
			return fallback;
		}
	}
	
	public static int parseInt(String string, int fallback)
	{
		try
		{
			return Integer.parseInt(string);
		}
		catch(NumberFormatException ex)
		{
			return fallback;
		}
	}
	
	public static short parseShort(String string, short fallback)
	{
		try
		{
			return Short.parseShort(string);
		}
		catch(NumberFormatException ex)
		{
			return fallback;
		}
	}
	
	public static byte parseByte(String string, byte fallback)
	{
		try
		{
			return Byte.parseByte(string);
		}
		catch(NumberFormatException ex)
		{
			return fallback;
		}
	}
	
	public static boolean parseBoolean(String string, boolean fallback)
	{
		try
		{
			return Boolean.parseBoolean(string);
		}
		catch(NumberFormatException ex)
		{
			return fallback;
		}
	}
	
	public static double parseDouble(String string, double fallback)
	{
		try
		{
			return Double.parseDouble(string);
		}
		catch(NumberFormatException ex)
		{
			return fallback;
		}
	}
	
	public static float parseFloat(String string, float fallback)
	{
		try
		{
			return Float.parseFloat(string);
		}
		catch(NumberFormatException ex)
		{
			return fallback;
		}
	}
}
