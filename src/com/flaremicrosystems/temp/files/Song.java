package com.flaremicrosystems.temp.files;

import java.io.File;

import com.flaremicrosystems.temp.PropertiesManager;

public class Song implements Comparable<Song>{
	public final File songFile;
	public final String title;
	public Song(File file) {
		songFile = file;
		String songTitle = file.getName().substring(0, file.getName().lastIndexOf(".")).trim();
		if(PropertiesManager.trimNames)
			songTitle = songTitle.substring(Math.max(file.getName().indexOf(" - "), 0)).replaceFirst(" - ", "");
		title = songTitle;
	}
	
	public int compareTo(Song o) {
		return title.compareTo(o.title);
	}
	
	@Override
	public boolean equals(Object object)
	{
		return (object instanceof Song && ((Song)object).songFile.equals(songFile));
	}

}
