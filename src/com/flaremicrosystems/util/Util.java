package com.flaremicrosystems.util;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;

/**
 * Utility class for useful functions.
 * 
 * @author Andreja
 *
 */
public class Util {

	// Most reliable way to get the jar's directory. (apparently)
	private static File baseDir = new File(decodeURL(Util.class.getProtectionDomain().getCodeSource().getLocation().getFile())).getParentFile();

	/**
	 * Get or create a configuration file from the config directory
	 * 
	 * @param filename
	 * @return configuration {@link File} that has been created if it did not
	 *         exist.
	 * @throws IOException
	 */
	public static File getConfigFile(String filename) throws IOException {
		return getConfigFile(filename, true);
	}

	public static String decodeURL(String url) {
		try {
			return URLDecoder.decode(url, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return url;
	}

	/**
	 * Get or create (if specified) a configuration file from the config
	 * directory
	 * 
	 * @param filename
	 * @param create
	 * @return configuration {@link File} that has been created if it did not
	 *         exist and creation was requested.
	 * @throws IOException
	 */
	public static File getConfigFile(String filename, boolean create) throws IOException {
		File file = new File(getConfigDirectory(), filename);
		if (!file.exists() && create)
			file.createNewFile();
		return file;
	}

	/**
	 * Get or create (if specified) a configuration file from the config
	 * directory
	 * 
	 * @param filename
	 * @param create
	 * @return configuration {@link File} that has been created if it did not
	 *         exist and creation was requested.
	 * @throws IOException
	 */
	public static File getConfigFileNoCreate(String filename) {
		File file = new File(getConfigDirectory(), filename);
		return file;
	}

	/**
	 * Get the config directory, and make any required directories if they do
	 * not exist
	 * 
	 * @return {@link File} config directory
	 */
	public static File getConfigDirectory() {
		File file = new File(getBaseDirectory(), "config");
		if (!file.isDirectory())
			file.mkdirs();
		return file;
	}
	
	/**
	 * Get the default music directory, will not make directories
	 * 
	 * @return {@link File} music directory
	 */
	public static File getDefaultMusicDirectory() {
		File file = new File(getBaseDirectory(), "Music");
		return file;
	}
	
	/**
	 * Get the bin directory, will not make directories
	 * 
	 * @return {@link File} bin directory
	 */
	public static File getBinDirectory() {
		File file = new File(getBaseDirectory(), "morebin");
		return file;
	}

	/**
	 * Get the base directory
	 * 
	 * @return {@link File} base directory
	 */
	public static File getBaseDirectory() {
		return baseDir;
	}

	/**
	 * Parse arguments, in arg=value format, or just arg for arguments with no
	 * value. Values can contain = signs, but not arguments.
	 * 
	 * @param args
	 * @param keyToLower
	 * @return
	 */
	public static HashMap<String, String> parseArgs(String args[], boolean keyToLower) {
		HashMap<String, String> map = new HashMap<String, String>();
		for (String s : args) {
			String[] res = s.split("=", 2);
			if (keyToLower)
				res[0].toLowerCase();
			if (res.length > 1) {
				map.put(res[0], res[1]);
			} else
				map.put(res[0], "");
		}
		return map;
	}

	public static boolean cleanClose(Closeable closeable) {
		try
		{
			if(closeable != null)
				closeable.close();
			return true;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
	}
}
