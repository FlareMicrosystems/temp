package com.flaremicrosystems.util;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Enumeration;

import javax.swing.UIDefaults;
import javax.swing.UIManager;

public class DPIUtil {
	private static float scale = Float.parseFloat(System.getProperty("sun.java2d.uiScale", "1"));

	public static void setNewScale(float scale) {
		DPIUtil.scale = scale;
	}

	public static Font getScaledFont(Font font) {
		return font.deriveFont(font.getStyle() | Font.BOLD, (int) (font.getSize() * scale));
	}

	public static Component setFontScale(Component c) {
		c.setFont(getScaledFont(c.getFont()));
		return c;
	}

	/**
	 * WAAAAAAAAAAY too many nested statements!!!
	 */
	public static void setDefaultUIFontsToScale() {
		UIDefaults def = UIManager.getLookAndFeelDefaults();
		Enumeration<Object> keys = def.keys();
		while (keys.hasMoreElements())
		{
			Object keyObject = keys.nextElement();
			if (keyObject instanceof String && ((String) keyObject).endsWith(".font"))
			{
				
				Object fontObject = def.get(keyObject);
				if (fontObject instanceof Font)
				{
					def.put(keyObject, getScaledFont((Font) fontObject));
				}
			}
		}
	}

	public static float getScale() {
		return scale;
	}
	
	public static int getScaledInt(int toScale) {
		return (int)(toScale*scale);
	}
	
	public static void scaleComponent(Component component) {
		component.setSize(new Dimension(getScaledInt(component.getWidth()), getScaledInt(component.getHeight())));
	}

	public static Dimension getScaledDimension(Dimension dimension) {
		return getScaledDimension(dimension.width, dimension.height);
	}

	public static Dimension getScaledDimension(int width, int height) {
		return new Dimension((int)Math.max(-1, width * scale), (int)Math.max(-1, height * scale));
	}

}
