package com.flaremicrosystems.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import javax.swing.JLabel;

public class BatteryUtil {
	public static BatteryInfo getBatteryInfo()
	{
		BufferedReader reader = null;
		try
		{
			Process process = Runtime.getRuntime().exec(new String[]{new File(Util.getBinDirectory(), "writebat.exe").toString()});
			reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			short lineStatus = Short.parseShort(reader.readLine());
			short batteryFlag = Short.parseShort(reader.readLine());
			short batteryLifePercent = Short.parseShort(reader.readLine());
			int batteryLifeTime = Short.parseShort(reader.readLine());
			int batteryFullLifeTime = Short.parseShort(reader.readLine());
			
			return new BatteryInfo(lineStatus, batteryFlag, batteryLifePercent, batteryLifeTime, batteryFullLifeTime);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Util.cleanClose(reader);
		}
		return new BatteryInfo((short)255, (short)255, (short)255, -1, -1);
	}
	
	public static void main(String args[])
	{
		BatteryInfo binfo = getBatteryInfo();
		System.out.printf("%d,%d,%d,%d,%d\n", binfo.lineStatus, binfo.batteryFlag, binfo.batteryLifePercent, binfo.batteryLifeTime, binfo.batteryFullLifeTime);
		System.out.println("Battery Percentage: " +binfo.batteryLifePercent+"%" + (binfo.batteryLifePercent == 255 ? " (There is not a valid battery!)" : ""));
	}

	public static BatteryThread startBatteryThread(JLabel lblBattery) {
		BatteryThread batteryThread = new BatteryThread(lblBattery);
		new Thread(batteryThread).start();
		return batteryThread;
	}
	
}

class BatteryInfo
{
	public static final int PLUGGED_OUT = 0;
	public static final int PLUGGED_IN = 0;
	
	public static final int UNKNOWN_VALUE = 255;
	public static final int UNKNOWN_TIME = -1;
	
	public static final int HIGH = 1;
	public static final int LOW = 2;
	public static final int CRITICAL = 4;
	public static final int CHARGING = 8;
	public static final int NO_BATT = 128;
	
	final public short lineStatus;
	final public short batteryFlag;
	final public short batteryLifePercent;
	final public int batteryLifeTime;
	final public int batteryFullLifeTime;
	
	BatteryInfo(short lineStatus, short batteryFlag, short batteryLifePercent, int batteryLifeTime, int batteryFullLifeTime)
	{
		this.lineStatus = lineStatus;
		this.batteryFlag = batteryFlag;
		this.batteryLifePercent = batteryLifePercent;
		this.batteryLifeTime = batteryLifeTime;
		this.batteryFullLifeTime = batteryFullLifeTime;
	}
	
}