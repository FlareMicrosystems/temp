package com.flaremicrosystems.temp.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import com.flaremicrosystems.temp.PropertiesManager;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.advanced.AdvancedPlayer;
import javazoom.jl.player.advanced.PlaybackListener;

public class ThreadedAudio implements Runnable {

	AdvancedPlayer player;
	private Thread thread;

	boolean running = true;
	boolean paused = false;

	public ThreadedAudio(AdvancedPlayer player2, boolean paused) {
		player = player2;
		this.paused = paused;
	}

	public void run() {
		while (running)
		{
			if (paused)
				try
				{
					Thread.sleep(100000000000L);
				}
				catch (InterruptedException e){}
			try
			{
				if (!player.play(PropertiesManager.frames))
					running = false;
			}
			catch (JavaLayerException e)
			{
				e.printStackTrace();
				//stop();
			}
			catch (NullPointerException ex)
			{

			}
		}
		player.stop();
	}

	public void stop() {
		running = false;
		thread.interrupt();
		try
		{
			thread.join();
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void pause() {
		paused = true;
	}

	public void unpause() {
		paused = false;
		thread.interrupt();
	}

	public static ThreadedAudio startPlaying(File file, PlaybackListener listener, boolean paused) {
		FileInputStream fis;
		try
		{
			fis = new FileInputStream(file);
			AdvancedPlayer player = new AdvancedPlayer(fis);
			player.setPlayBackListener(listener);
			ThreadedAudio threadedAudio = new ThreadedAudio(player, paused);
			threadedAudio.thread = new Thread(threadedAudio);
			threadedAudio.thread.start();
			return threadedAudio;
		}
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JavaLayerException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
