package com.flaremicrosystems.temp.ui.dialog;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.SystemColor;

public class ScalingDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final JPanel contentPanel = new JPanel();

	public static final int YES_OPTION = 0;
	public static final int NO_OPTION = 1;
	public static final int CANCEL_OPTION = 2;
	public static final int YES_NO_OPTION = 3;
	public static final int YES_NO_CANCEL_OPTION = 4;
	
	private int returnIndex = -1;

	/**
	 * @wbp.parser.constructor
	 */
	private ScalingDialog(Frame parent, String title, String message, String[] options, int[] indecies) {
		super(parent);
		if(parent == null)
			setAlwaysOnTop(true);
		make(parent, title, message, options, indecies);
	}
	
	private ScalingDialog(Dialog parent, String title, String message, String[] options, int[] indecies)
	{
		super(parent);
		make(parent, title, message, options, indecies);
	}
	
	private void make(Component parent, String title, String message, final String[] options, final int[] indecies)
	{
		setTitle(title);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JLabel lblNewLabel = new JLabel(message);
		contentPanel.add(lblNewLabel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBackground(SystemColor.activeCaptionBorder);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT, 25, 15));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			for(int i = 0; i < options.length; i++)
			{
				final int fi = i;
				JButton button = new JButton(options[i]);
				buttonPane.add(button);
				button.addActionListener(new ActionListener(){

					public void actionPerformed(ActionEvent e) {
						if(indecies != null && indecies.length > fi)
							returnIndex = indecies[fi];
						else returnIndex = fi;
						dispose();
					}
					
				});
			}
		}
		setModal(true);
		this.setMinimumSize(new Dimension(240, 240));
		pack();
		setLocationRelativeTo(parent);
	}
	
	public static int showConfirmDialog(Component parent, String message, String title, int option)
	{
		String[] options = null;
		int[] indecies = null;
		switch(option)
		{
			case YES_OPTION:
				options = new String[]{"Yes"};
				indecies = new int[]{YES_OPTION};
				break;
			case NO_OPTION:
				options = new String[]{"No"};
				indecies = new int[]{NO_OPTION};
				break;
			case CANCEL_OPTION:
				options = new String[]{"Cancel"};
				indecies = new int[]{CANCEL_OPTION};
				break;
			case YES_NO_OPTION:
				options = new String[]{"Yes", "No"};
				indecies = new int[]{YES_OPTION, NO_OPTION};
				break;
			default:
				options = new String[]{"Yes", "No", "Cancel"};
				indecies = new int[]{YES_OPTION, NO_OPTION, CANCEL_OPTION};
				break;
		}
		ScalingDialog dialog;
		if(parent instanceof Frame)
			dialog = new ScalingDialog((Frame)parent, title, message, options, indecies);
		else if(parent instanceof Dialog)
			dialog = new ScalingDialog((Dialog)parent, title, message, options, indecies);
		else
			dialog = new ScalingDialog((Frame)null, title, message, options, indecies);
		
		dialog.setVisible(true);
		
		return dialog.returnIndex;
	}

}
