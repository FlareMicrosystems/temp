package com.flaremicrosystems.temp.ui;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

import com.flaremicrosystems.temp.files.Album;
import com.flaremicrosystems.temp.files.Song;
import com.flaremicrosystems.util.DPIUtil;

import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.JLabel;

public class AlbumPanelUgly extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Create the panel.
	 */

	JScrollPane scrollPane = new JScrollPane();
	JToggleButton btn;
	
	public AlbumPanelUgly(final Album album, final MainFrame mainFrame) {

		setOpaque(false);
		setBorder(new EmptyBorder(10, 10, 10, 10));
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel_3 = new JPanel();
		panel_3.setOpaque(false);
		add(panel_3, BorderLayout.CENTER);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(new VerticalLayout());
		panel_3.add(scrollPane);
		scrollPane.setViewportView(panel_2);
		scrollPane.setVisible(false);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		JPanel panel = new JPanel();
		panel_3.add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		btn = new JToggleButton(album.albumTitle + "        (" + album.songs.size() + " song"+ (album.songs.size() == 1 ? "" : "s") +")");
		//if(album.albumArt != null)
			//btn.setIcon();
		
		panel.add(btn, BorderLayout.CENTER);
		panel.setOpaque(false);
		btn.setHorizontalAlignment(SwingConstants.LEFT);
		btn.setPreferredSize(DPIUtil.getScaledDimension(new Dimension(-1, 24)));
		
		JPanel panel_1 = new JPanel();
		panel_1.setOpaque(false);
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setVgap(0);
		flowLayout.setHgap(0);
		panel_1.setBorder(new EmptyBorder(0, 0, 0, 0));
		panel.add(panel_1, BorderLayout.EAST);
		
		JButton btnAddAll = new JButton("Add All");
		btnAddAll.setPreferredSize(DPIUtil.getScaledDimension(new Dimension(64, 24)));
		btnAddAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.addToPlaylist(new ArrayList<Song>(album.songs), -1);
			}
		});
		
		JLabel lblNewLabel = new JLabel("");
		panel_1.add(lblNewLabel);
		Image image = album.albumArt;
		if(album.albumArt == null)
		{
			image = new BufferedImage(1, 1, BufferedImage.TYPE_4BYTE_ABGR);
		}
		
		for(Song song : album.songs)
		{
			panel_2.add(new SongPanel(song, mainFrame));
		}
		lblNewLabel.setIcon(new ImageIcon(image.getScaledInstance(DPIUtil.getScaledInt(22), -1, Image.SCALE_SMOOTH)));
		panel_1.add(btnAddAll);
		btn.addActionListener(this);
		
	}

	public void actionPerformed(ActionEvent e) {
		scrollPane.setVisible(btn.isSelected());
		revalidate();
	}
}
