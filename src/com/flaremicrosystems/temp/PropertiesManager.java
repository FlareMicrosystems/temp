package com.flaremicrosystems.temp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import com.flaremicrosystems.util.NumberUtil;
import com.flaremicrosystems.util.Util;

public class PropertiesManager {
	
	private PropertiesManager()
	{}
	
	private static Properties properties = new Properties();
	public static File musicDirectory = Util.getDefaultMusicDirectory();
	public static float scaling = 1.0F;
	public static int frames = 8;
	public static boolean fullScreen = true;
	public static boolean trimNames = true;
	public static boolean loopPlaylist = false;
	public static long navThreshold = 0;

	public static boolean init()
	{
		return loadProperties();
	}
	
	public static boolean flush()
	{
		return storeProperties();
	}
	
	public static boolean loadProperties() {
		setProperties();
		File propertiesFile = Util.getConfigFileNoCreate("properties");
		if (propertiesFile.exists())
		{
			FileInputStream reader = null;
			try
			{
				reader = new FileInputStream(propertiesFile);
				properties.load(reader);
				parseProperties();
				return true;
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				Util.cleanClose(reader);
			}
		}
		return false;
	}

	public static boolean storeProperties() {
		setProperties();
		File propertiesFile = Util.getConfigFileNoCreate("properties");
		FileOutputStream writer = null;
		try
		{
			if(!propertiesFile.exists())
				propertiesFile.createNewFile();
			writer = new FileOutputStream(propertiesFile);
			properties.store(writer, "");
			return true;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			Util.cleanClose(writer);
		}
		return false;
	}

	private static void parseProperties() {
		musicDirectory = new File(properties.getProperty("music-dir", musicDirectory.toString()));
		scaling = NumberUtil.parseFloat(properties.getProperty("scale", String.valueOf(scaling)), scaling);
		frames = NumberUtil.parseInt(properties.getProperty("buffer", String.valueOf(frames)), frames);
		navThreshold = NumberUtil.parseLong(properties.getProperty("nav-threshold", String.valueOf(navThreshold)), navThreshold);
		fullScreen = NumberUtil.parseBoolean(properties.getProperty("full-screen", String.valueOf(fullScreen)), fullScreen);
		trimNames = NumberUtil.parseBoolean(properties.getProperty("trim-names", String.valueOf(trimNames)), trimNames);
		loopPlaylist = NumberUtil.parseBoolean(properties.getProperty("loop-playlist", String.valueOf(loopPlaylist)), loopPlaylist);
	}

	public static void setProperties() {
		properties.setProperty("music-dir", musicDirectory.toString());
		properties.setProperty("scale", String.valueOf(scaling));
		properties.setProperty("buffer", String.valueOf(frames));
		properties.setProperty("full-screen", String.valueOf(fullScreen));
		properties.setProperty("trim-names", String.valueOf(trimNames));
		properties.setProperty("loop-playlist", String.valueOf(loopPlaylist));
		properties.setProperty("nav-threshold", String.valueOf(navThreshold));
	}
}
