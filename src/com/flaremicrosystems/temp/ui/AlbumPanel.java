package com.flaremicrosystems.temp.ui;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

import com.flaremicrosystems.temp.files.Album;
import com.flaremicrosystems.temp.files.Song;
import com.flaremicrosystems.util.DPIUtil;

import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JScrollPane;

public class AlbumPanel extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Create the panel.
	 */

	JScrollPane scrollPane = new JScrollPane();
	JToggleButton btn;
	MainFrame mainFrame;
	
	public AlbumPanel(final Album album, final MainFrame mainFrame) {

		this.mainFrame = mainFrame;
		setOpaque(false);
		setBorder(new EmptyBorder(10, 10, 10, 10));
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		btn = new JToggleButton(album.albumTitle + "        (" + album.songs.size() + " song"+ (album.songs.size() == 1 ? "" : "s") +")");
		//if(album.albumArt != null)
			//btn.setIcon(new ImageIcon(album.albumArt.getScaledInstance(-1, DPIUtil.getScaledInt(22), Image.SCALE_SMOOTH)));
		panel.add(btn, BorderLayout.CENTER);
		panel.setOpaque(false);
		btn.setHorizontalAlignment(SwingConstants.LEFT);
		btn.setPreferredSize(DPIUtil.getScaledDimension(new Dimension(-1, 24)));
		
		JPanel panel_1 = new JPanel();
		panel_1.setOpaque(false);
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setVgap(0);
		flowLayout.setHgap(0);
		panel_1.setBorder(new EmptyBorder(0, 20, 0, 0));
		panel.add(panel_1, BorderLayout.EAST);
		
		JButton btnAddAll = new JButton("Add All");
		btnAddAll.setPreferredSize(DPIUtil.getScaledDimension(new Dimension(64, 24)));
		btnAddAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.addToPlaylist(new ArrayList<Song>(album.songs), -1);
			}
		});
		panel_1.add(btnAddAll);
		btn.addActionListener(this);
		
		add(scrollPane, BorderLayout.CENTER);
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(new VerticalLayout());
		scrollPane.setViewportView(panel_2);
		scrollPane.setVisible(false);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		for(Song song : album.songs)
		{
			panel_2.add(new SongPanel(song, mainFrame));
		}
	}

	public void actionPerformed(ActionEvent e) {
		scrollPane.setVisible(btn.isSelected());
		revalidate();
		mainFrame.updateScrollButtons();
	}
}
