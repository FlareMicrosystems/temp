package com.flaremicrosystems.temp.ui;

import javax.swing.JPanel;
import javax.swing.JToggleButton;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.SwingConstants;

import com.flaremicrosystems.temp.files.Song;
import com.flaremicrosystems.util.DPIUtil;
import com.flaremicrosystems.util.DislikeUtil;

import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.GridLayout;
import java.util.Arrays;
import java.awt.Color;

public class SongPanel extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the panel.
	 * @param mainFrame 
	 */
	
	MainFrame mainFrame;
	
	Song song;
	JButton btnAddToPlaylist = new JButton("Add");
	JButton btn;
	JToggleButton btnDislike;
	
	Color goodColor = new Color(183, 255, 190);
	Color badColor =new Color(255, 183, 190);
	
	public SongPanel(final Song song, final MainFrame mainFrame) {
		setOpaque(false);
		setBorder(new EmptyBorder(10, 10, 10, 10));
		setLayout(new BorderLayout(0, 0));
		
		this.mainFrame = mainFrame;
		this.song = song;
		
		btn = new JButton(song.title);
		btn.setBackground(new Color(255, 255, 204));
		btn.setPreferredSize(new Dimension(-1, -1));
		add(btn, BorderLayout.CENTER);
		btn.setHorizontalAlignment(SwingConstants.LEFT);
		btn.addActionListener(this);
		
		JPanel panel = new JPanel();
		panel.setOpaque(false);
		panel.setBorder(new EmptyBorder(0, 10, 0, 0));
		add(panel, BorderLayout.EAST);
		panel.setLayout(new GridLayout(1, 0, 10, 0));
		btnAddToPlaylist.setBackground(new Color(255, 255, 204));

		btnAddToPlaylist.addActionListener(this);
		panel.add(btnAddToPlaylist);
		
		btnDislike = new JToggleButton();
		btnDislike.addActionListener(this);
		btnDislike.setSelected(DislikeUtil.isDisliked(song));
		btnDislike.setPreferredSize(DPIUtil.getScaledDimension(64, -1));
		btnDislike.setBackground(btnDislike.isSelected() ? badColor : goodColor);
		btnDislike.setText(btnDislike.isSelected() ? "Bad" : "Good");
		panel.add(btnDislike);
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btn)
		{
			int idx = mainFrame.addToPlaylist(Arrays.asList(new Song[]{song}), -2);
			mainFrame.play(idx);
		}
		else if(e.getSource() == btnAddToPlaylist)
		{
			mainFrame.addToPlaylist(Arrays.asList(new Song[]{song}), -1);
		}
		else if(e.getSource() == btnDislike)
		{
			btnDislike.setBackground(btnDislike.isSelected() ? badColor : goodColor);
			btnDislike.setText(btnDislike.isSelected() ? "Bad" : "Good");
			DislikeUtil.setDisliked(song, btnDislike.isSelected());
		}
	}
}
