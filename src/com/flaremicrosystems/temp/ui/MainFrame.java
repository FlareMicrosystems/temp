package com.flaremicrosystems.temp.ui;

import java.awt.BorderLayout;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JScrollPane;

import javazoom.jl.player.advanced.PlaybackEvent;
import javazoom.jl.player.advanced.PlaybackListener;

import com.flaremicrosystems.temp.PropertiesManager;
import com.flaremicrosystems.temp.controller.ThreadedAudio;
import com.flaremicrosystems.temp.files.Album;
import com.flaremicrosystems.temp.files.FileUtils;
import com.flaremicrosystems.temp.files.Song;
import com.flaremicrosystems.temp.ui.dialog.ConfigurationDialog;
import com.flaremicrosystems.util.BatteryThread;
import com.flaremicrosystems.util.BatteryUtil;
import com.flaremicrosystems.util.DPIUtil;
import com.flaremicrosystems.util.VolumeControl;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ScrollPaneConstants;

import java.awt.SystemColor;

import javax.swing.ImageIcon;

public class MainFrame extends JFrame implements PlaybackListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	public BatteryThread batteryThread;
	
	List<Song> playlist = new LinkedList<Song>();
	ThreadedAudio threadedAudio = null;
	JPanel playlistPanel = new JPanel();
	JScrollPane scrollPane_1 = new JScrollPane();		
	final JScrollPane scrollPane = new JScrollPane();

	final JButton btnScrollUpPlaylist = new JButton("\u25b2");
	final JButton btnScrollDownPlaylist = new JButton("\u25bc");
	

	final JButton btnScrollUpMusic = new JButton("\u25b2");
	final JButton btnScrollDownMusic = new JButton("\u25bc");
	

	JPanel pnlMusicContainer = new JPanel();
	
	boolean stopped = true;
	boolean paused = false;
	boolean shuttingDown = false;
	int currPos = 0;
	long lastNavTime = 0;

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		super("TEMP");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 598, 454);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaptionBorder);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		panel.setOpaque(false);
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BorderLayout(0, 0));

		JPanel panel_7 = new JPanel();
		panel_7.setOpaque(false);
		FlowLayout flowLayout_1 = (FlowLayout) panel_7.getLayout();
		flowLayout_1.setVgap(15);
		flowLayout_1.setHgap(20);
		panel.add(panel_7, BorderLayout.WEST);

		final JButton btnShutdown = new JButton("Turn Off");
		btnShutdown.setPreferredSize(DPIUtil.getScaledDimension(70, 24));
		btnShutdown.addMouseListener(new MouseAdapter() {
			
			Thread t = null;
			int nClicks = 0;
			public void mousePressed(MouseEvent e) {
				if(t != null)
					t.interrupt();
				t = null;
				nClicks++;
				if(nClicks < 3)
				{
					btnShutdown.setText(3 - nClicks + "");
					t = new Thread(){
						public void run(){
							try
							{
								Thread.sleep(2000L);
								nClicks = 0;
								btnShutdown.setText("Turn Off");
							}
							catch (InterruptedException e){}
						}
					};
					t.start();
				}
				else
				{
					nClicks = 0;
					btnShutdown.setText("Turn Off");
					try
					{
						Runtime.getRuntime().exec(new String[] {"shutdown", "/s", "/t", "0"});
						System.exit(0);
					}
					catch (IOException e1){e1.printStackTrace();}	
				}
			}
			
			/*public void mousePressed(MouseEvent e) {
				if(e.getClickCount() >= 3 && ScalingDialog.showConfirmDialog(MainFrame.this, "Are you sure you want to turn the jukebox off?", "Turn Off JukeBox", ScalingDialog.YES_NO_OPTION) == ScalingDialog.YES_OPTION)
				{
					try
					{
						Runtime.getRuntime().exec(new String[] {"shutdown", "/s", "/t", "0"});
						System.exit(0);
					}
					catch (IOException e1){e1.printStackTrace();}
				}
			}*/
		});
		btnShutdown.setIcon(null);
		panel_7.add(btnShutdown);

		JPanel panel_8 = new JPanel();
		panel_8.setOpaque(false);
		FlowLayout flowLayout_2 = (FlowLayout) panel_8.getLayout();
		flowLayout_2.setVgap(15);
		flowLayout_2.setHgap(20);
		panel.add(panel_8, BorderLayout.EAST);

		final JButton btnConfigure = new JButton("Settings");
		btnConfigure.setPreferredSize(DPIUtil.getScaledDimension(70, 24));
		btnConfigure.addMouseListener(new MouseAdapter() {
			Thread t = null;
			int nClicks = 0;
			public void mousePressed(MouseEvent e) {
				if(t != null)
					t.interrupt();
				t = null;
				nClicks++;
				if(nClicks < 3)
				{
					btnConfigure.setText(3 - nClicks + "");
					t = new Thread(){
						public void run(){
							try
							{
								Thread.sleep(2000L);
								nClicks = 0;
								btnConfigure.setText("Settings");
							}
							catch (InterruptedException e){}
						}
					};
					t.start();
				}
				else
				{
					nClicks = 0;
					btnConfigure.setText("Settings");
					new ConfigurationDialog(MainFrame.this).setVisible(true);	
				}
			}
		});
		panel_8.add(btnConfigure);

		JPanel panel_9 = new JPanel();
		panel_9.setOpaque(false);
		FlowLayout flowLayout = (FlowLayout) panel_9.getLayout();
		flowLayout.setVgap(15);
		flowLayout.setHgap(60);
		panel.add(panel_9, BorderLayout.CENTER);

		JButton btnStop = new JButton("Stop");
		btnStop.setPreferredSize(DPIUtil.getScaledDimension(new Dimension(75, 24)));
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (threadedAudio != null)
				{
					threadedAudio.pause();
					paused = true;
				}
			}
		});
		panel_9.add(btnStop);

		JButton btnPlay = new JButton("Play");
		btnPlay.setPreferredSize(DPIUtil.getScaledDimension(new Dimension(75, 24)));
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (threadedAudio != null)
				{
					threadedAudio.unpause();
					paused = false;
				}
			}
		});
		panel_9.add(btnPlay);
		
		JPanel panel_10 = new JPanel();
		panel_9.add(panel_10);
		
		JButton btnVolMinus = new JButton("");
		try
		{
			btnVolMinus.setIcon(new ImageIcon(ImageIO.read(MainFrame.class.getResource("/icons/minus.png")).getScaledInstance(DPIUtil.getScaledInt(10), DPIUtil.getScaledInt(10), Image.SCALE_FAST)));
		}
		catch (IOException e1){	e1.printStackTrace();}
		btnVolMinus.setFont(btnVolMinus.getFont().deriveFont(btnVolMinus.getFont().getSize() + 10));
		btnVolMinus.setPreferredSize(DPIUtil.getScaledDimension(36, 24));
		btnVolMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VolumeControl.changeVol(-65525/10);
			}
		});
		panel_10.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panel_10.add(btnVolMinus);
		
		JButton btnVolPlus = new JButton("");
		try
		{
			btnVolPlus.setIcon(new ImageIcon(ImageIO.read(MainFrame.class.getResourceAsStream("/icons/plus.png")).getScaledInstance(DPIUtil.getScaledInt(10), DPIUtil.getScaledInt(10), Image.SCALE_FAST)));
		}
		catch (IOException e1){	e1.printStackTrace();}
		btnVolPlus.setFont(btnVolMinus.getFont());
		btnVolPlus.setPreferredSize(DPIUtil.getScaledDimension(36, 24));
		btnVolPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VolumeControl.changeVol(65525/10);
			}
		});
		
		JLabel lblVolume = new JLabel("Volume");
		panel_10.add(lblVolume);
		panel_10.add(btnVolPlus);

		JPanel panel_1 = new JPanel();
		panel_1.setPreferredSize(DPIUtil.getScaledDimension(new Dimension(200, -1)));
		panel_1.setOpaque(false);
		contentPane.add(panel_1, BorderLayout.EAST);
		panel_1.setLayout(new BorderLayout(0, 0));

		JPanel panel_3 = new JPanel();
		panel_3.setOpaque(false);
		panel_1.add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));

		JLabel lblPlaylist = new JLabel("Playlist");
		lblPlaylist.setHorizontalAlignment(SwingConstants.CENTER);
		panel_3.add(lblPlaylist, BorderLayout.NORTH);

		JPanel panel_5 = new JPanel();
		panel_5.setOpaque(false);
		panel_3.add(panel_5, BorderLayout.CENTER);
		panel_5.setLayout(new BorderLayout(0, 0));

		btnScrollUpPlaylist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lastNavTime = System.currentTimeMillis();
				scrollPane_1.getVerticalScrollBar().setValue(scrollPane_1.getVerticalScrollBar().getValue() - DPIUtil.getScaledInt(128));
				btnScrollUpPlaylist.setEnabled(scrollPane_1.getVerticalScrollBar().getValue() > 0);
				btnScrollDownPlaylist.setEnabled(scrollPane_1.getVerticalScrollBar().getValue() < (scrollPane_1.getVerticalScrollBar().getMaximum() - scrollPane_1.getVerticalScrollBar().getVisibleAmount()));
			}
		});
		panel_5.add(btnScrollUpPlaylist, BorderLayout.NORTH);

		btnScrollDownPlaylist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lastNavTime = System.currentTimeMillis();
				scrollPane_1.getVerticalScrollBar().setValue(scrollPane_1.getVerticalScrollBar().getValue() + DPIUtil.getScaledInt(128));
				btnScrollUpPlaylist.setEnabled(scrollPane_1.getVerticalScrollBar().getValue() > 0);
				btnScrollDownPlaylist.setEnabled(scrollPane_1.getVerticalScrollBar().getValue() < (scrollPane_1.getVerticalScrollBar().getMaximum() - scrollPane_1.getVerticalScrollBar().getVisibleAmount()));
			}
		});
		panel_5.add(btnScrollDownPlaylist, BorderLayout.SOUTH);

		scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		panel_5.add(scrollPane_1, BorderLayout.CENTER);
		playlistPanel.setBackground(SystemColor.controlShadow);

		playlistPanel.setLayout(new VerticalLayout());
		scrollPane_1.setViewportView(playlistPanel);

		JPanel panel_2 = new JPanel();
		panel_2.setOpaque(false);
		contentPane.add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new BorderLayout(0, 0));

		JLabel lblNewLabel = new JLabel("Song Library");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panel_2.add(lblNewLabel, BorderLayout.NORTH);

		JPanel panel_4 = new JPanel();
		panel_4.setOpaque(false);
		panel_2.add(panel_4, BorderLayout.CENTER);
		panel_4.setLayout(new BorderLayout(0, 0));

		panel_4.add(btnScrollUpMusic, BorderLayout.NORTH);

		panel_4.add(btnScrollDownMusic, BorderLayout.SOUTH);


		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		panel_4.add(scrollPane, BorderLayout.CENTER);

		
		btnScrollUpMusic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getValue() - DPIUtil.getScaledInt(128));
				btnScrollUpMusic.setEnabled(scrollPane.getVerticalScrollBar().getValue() > 0);
				btnScrollDownMusic.setEnabled(scrollPane.getVerticalScrollBar().getValue() < (scrollPane.getVerticalScrollBar().getMaximum() - scrollPane.getVerticalScrollBar().getVisibleAmount()));
			}
		});
		panel_5.add(btnScrollUpPlaylist, BorderLayout.NORTH);

		btnScrollDownMusic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getValue() + DPIUtil.getScaledInt(128));
				btnScrollUpMusic.setEnabled(scrollPane.getVerticalScrollBar().getValue() > 0);
				btnScrollDownMusic.setEnabled(scrollPane.getVerticalScrollBar().getValue() < (scrollPane.getVerticalScrollBar().getMaximum() - scrollPane.getVerticalScrollBar().getVisibleAmount()));
			}
		});
		
		pnlMusicContainer.setBackground(SystemColor.controlShadow);
		pnlMusicContainer.setLayout(new VerticalLayout());
		scrollPane.setViewportView(pnlMusicContainer);

		loadMusic();
		
		btnScrollUpPlaylist.setEnabled(false);
		btnScrollDownPlaylist.setEnabled(false);
		
		JLabel lblBattery = new JLabel("Battery");
		lblBattery.setHorizontalAlignment(SwingConstants.TRAILING);
		contentPane.add(lblBattery, BorderLayout.NORTH);
		batteryThread = BatteryUtil.startBatteryThread(lblBattery);
	}

	public void loadMusic() {
		pnlMusicContainer.removeAll();
		TreeSet<Album> albums = FileUtils.getAlbumsInDir(PropertiesManager.musicDirectory);
		for (Album album : albums)
		{
			AlbumPanel button = new AlbumPanel(album, this);
			pnlMusicContainer.add(button);
		}
		updateScrollButtons();
	}

	public int addToPlaylist(List<Song> songs, int position) {
		if (position == -1)
			position = playlist.size();
		else if (position == -2 && currPos < playlist.size())
		{
			position = currPos + 1;
			paused = false;
		}
		else if (position == -2)
		{
			position = currPos;
			paused = false;
		}
		playlist.addAll(position, songs);
		if (stopped)
			play(position);
		makePlaylistPanel();
		return position;
	}

	private void makePlaylistPanel() {
		playlistPanel.removeAll();
		for (int i = 0; i < playlist.size(); i++)
		{
			PlaylistEntry tButton = new PlaylistEntry(playlist.get(i), i, this);
			tButton.setSelected(currPos == i);
			playlistPanel.add(tButton);
		}
		playlistPanel.revalidate();
		playlistPanel.repaint();
		scrollPane_1.revalidate();
		scrollPane_1.validate();
		scrollPane_1.repaint();
		contentPane.revalidate();
		btnScrollUpPlaylist.setEnabled(scrollPane_1.getVerticalScrollBar().getValue() > 0);
		btnScrollDownPlaylist.setEnabled(scrollPane_1.getVerticalScrollBar().getValue() < (scrollPane_1.getVerticalScrollBar().getMaximum() - scrollPane_1.getVerticalScrollBar().getVisibleAmount()));
	}

	public void play(final int index) {
		currPos = index;
		for (int i = 0; i < playlistPanel.getComponentCount(); i++)
		{
			((PlaylistEntry) playlistPanel.getComponents()[i]).setSelected(i == currPos);
			if(i == currPos)
			{
				PlaylistEntry entry = (PlaylistEntry) playlistPanel.getComponents()[i];
				if(PropertiesManager.navThreshold == 0 || lastNavTime < System.currentTimeMillis() - PropertiesManager.navThreshold)
				{
					int topoff = entry.getLocation().y - scrollPane_1.getVerticalScrollBar().getValue();
					int bottomoff = (scrollPane_1.getVerticalScrollBar().getValue()+scrollPane_1.getVerticalScrollBar().getVisibleAmount()) - (entry.getLocation().y+entry.getHeight());
					if(topoff < 0)
						scrollPane_1.getVerticalScrollBar().setValue(scrollPane_1.getVerticalScrollBar().getValue() + topoff);
					if(bottomoff < 0)
						scrollPane_1.getVerticalScrollBar().setValue(scrollPane_1.getVerticalScrollBar().getValue() - bottomoff);	
					
					btnScrollUpPlaylist.setEnabled(scrollPane_1.getVerticalScrollBar().getValue() > 0);
					btnScrollDownPlaylist.setEnabled(scrollPane_1.getVerticalScrollBar().getValue() < (scrollPane_1.getVerticalScrollBar().getMaximum() - scrollPane_1.getVerticalScrollBar().getVisibleAmount()));
				}
			}
		}
		stopped = true;
		if (threadedAudio != null)
			threadedAudio.stop();
		stopped = false;
		threadedAudio = ThreadedAudio.startPlaying(playlist.get(currPos).songFile, this, paused);
	}

	public void playbackStarted(PlaybackEvent evt) {
		
	}

	public void playbackFinished(PlaybackEvent evt) {
		if (!stopped)
		{
			currPos++;
			if (playlist.size() > currPos)
				play(currPos);
			else if(PropertiesManager.loopPlaylist)
			{
				currPos = 0;
				play(currPos);
			}
			else 
				stopped = true;
		}
	}

	public void removeSong(int i) {
		playlist.remove(i);
		if (currPos == i)
		{
			stopped = true;
			if (threadedAudio != null)
				threadedAudio.stop();
			if (playlist.size() > 0)
			{
				currPos = currPos % playlist.size();
				if(currPos != 0 || PropertiesManager.loopPlaylist)
				play(currPos);
			}
		}
		else if(currPos > i)
		{
			currPos--;
		}
		makePlaylistPanel();
	}

	//Possibly too much validating and painting, but it was bugging out for some reason without all this
	public void updateScrollButtons() {
		pnlMusicContainer.revalidate();
		pnlMusicContainer.validate();
		scrollPane.revalidate();
		scrollPane.validate();
		scrollPane.repaint();
		btnScrollUpMusic.setEnabled(scrollPane.getVerticalScrollBar().getValue() > 0);
		btnScrollDownMusic.setEnabled(scrollPane.getVerticalScrollBar().getValue() < (scrollPane.getVerticalScrollBar().getMaximum() - scrollPane.getVerticalScrollBar().getVisibleAmount()));
	}

	public void setFullScreen(boolean fullScreen) {
		//if(this.isDisplayable())
		dispose();
		setUndecorated(fullScreen);
		setExtendedState(fullScreen ? JFrame.MAXIMIZED_BOTH : JFrame.NORMAL);
		setVisible(true);
	}

	public void clearPlaylist() {
		stopped = true;
		if (threadedAudio != null)
			threadedAudio.stop();
		threadedAudio = null;
		playlist.clear();
		makePlaylistPanel();
	}

}
