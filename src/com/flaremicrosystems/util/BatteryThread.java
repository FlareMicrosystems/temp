package com.flaremicrosystems.util;

import javax.swing.JLabel;

public class BatteryThread implements Runnable
{
	boolean stopped = false;
	JLabel toUpdate;
	public BatteryThread(JLabel lblBattery) {
		toUpdate = lblBattery;
	}
	
	public void stop()
	{
		stopped = true;
	}
	
	public void run() {
		while(!stopped)
		{
			try
			{
				final BatteryInfo battInfo = BatteryUtil.getBatteryInfo();
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						String percentText = "";
						if(battInfo.batteryLifePercent <= 100) 
						{
							percentText = battInfo.batteryLifePercent+"%";
							if((battInfo.batteryFlag & BatteryInfo.CHARGING) != 0)
							{
								percentText = "Charging: " + percentText;
							}
							else if(battInfo.lineStatus == 1)
							{
								percentText = "Charged: " + percentText;
							}
						}
						toUpdate.setText(percentText);
					}
				});
				Thread.sleep(5000L);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
	
}