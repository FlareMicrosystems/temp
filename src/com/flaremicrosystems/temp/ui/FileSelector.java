package com.flaremicrosystems.temp.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.ItemSelectable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

import com.flaremicrosystems.util.Util;


public class FileSelector extends Container implements ItemSelectable {
	private static final long serialVersionUID = 1L;
	ArrayList<FileFilter> fileFilters = new ArrayList<FileFilter>();
	FileFilter defaultFileFilter = null;
	private JTextField txtFilename;
	private File selectedFile;
	private ArrayList<ItemListener> listeners = new ArrayList<ItemListener>();
	boolean isRelative = true;
	private JButton btnSelect = new JButton("Select...");
	public FileSelector()
	{
		this(Util.getBaseDirectory());
	}
	
	public FileSelector(File file)
	{
		this(file ,true);
	}
	
	
	public FileSelector(File file, boolean isRelative) {
		selectedFile = file;
		setLayout(new BorderLayout(0, 0));
		
		add(btnSelect, BorderLayout.WEST);
		txtFilename = new JTextField();
		txtFilename.setEditable(false);
		add(txtFilename, BorderLayout.CENTER);
		txtFilename.setColumns(10);
		btnSelect.addActionListener(new SelectionActionListener(this));
		this.isRelative = isRelative;
		updateName();
	}
	
	public void setDefaultFileFilter(FileFilter defaultFileFilter)
	{
		this.defaultFileFilter = defaultFileFilter;
	}
	
	public void addFileFilter(FileFilter fileFilter)
	{
		this.fileFilters.add(fileFilter);
	}
	
	public final void setRelative(boolean relative)
	{
		isRelative = relative;
		updateName();
	}
	
	protected static final String tryGetRelativePath(File file)
	{
		URI base = Util.getBaseDirectory().toURI();
		URI relativePath = base.relativize(file.toURI());
		if(relativePath.getPath().startsWith("/"))
			return file.getAbsolutePath();
		return relativePath.getPath();
	}
	
	public String getRelativePath() {
		return tryGetRelativePath(selectedFile);
	}
	
	public void setFile(File file){
		this.selectedFile = file;
		if(isRelative)
		{
			setToolTipText(tryGetRelativePath(file));
		}
		else 
			setToolTipText(file.getAbsoluteFile().toString());
		updateName();
		validate();
		repaint();
		
		ItemEvent e = new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED, file, ItemEvent.ITEM_STATE_CHANGED);
		for(ItemListener i : listeners)
		{
			i.itemStateChanged(e);
		}
	}
	
	public File getFile(){
		return this.selectedFile;
	}
	
	public String getFileString()
	{
		if(isRelative)
			return tryGetRelativePath(selectedFile);
		else return selectedFile.getAbsoluteFile().toString();
	}
	
	protected final void updateName()
	{
		String name;
		if(isRelative)
			name = tryGetRelativePath(selectedFile);
		else name = selectedFile.getAbsoluteFile().toString();
		txtFilename.setText(name);
	}
	
	public final void addItemListener(ItemListener l)
	{
		listeners.add(l);
	}

	public void setToolTipText(String string) {
		btnSelect.setToolTipText(string);
		txtFilename.setToolTipText(string);
	}

	public Object[] getSelectedObjects() {
		return new Object[]{selectedFile};
	}

	public void removeItemListener(ItemListener arg0) {
		listeners.remove(arg0);
	}

}

class SelectionActionListener implements ActionListener
{
	private final FileSelector fs;
	public SelectionActionListener(final FileSelector fs)
	{
		this.fs = fs;
	}
	public void actionPerformed(ActionEvent e) {
		JFileChooser chooser = new JFileChooser(fs.getFile());
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		if(fs.defaultFileFilter != null)
			chooser.setFileFilter(fs.defaultFileFilter);
		for(FileFilter f : fs.fileFilters)
		{
			chooser.addChoosableFileFilter(f);
		}
		if(chooser.showOpenDialog(fs) == JFileChooser.APPROVE_OPTION)
		{
			fs.setFile(chooser.getSelectedFile());
		}
	}
	
}
