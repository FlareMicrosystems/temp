package com.flaremicrosystems.util;

import java.io.File;
import java.io.IOException;

import com.flaremicrosystems.temp.files.Song;

public class DislikeUtil {
	public static File getDislikeFile(Song song)
	{
		return new File(song.songFile.toString() + ".dislike");
	}
	
	public static boolean isDisliked(Song song)
	{
		return getDislikeFile(song).exists();
	}
	
	public static void setDisliked(Song song, boolean disliked)
	{
		File file = getDislikeFile(song);
		if(disliked)
			try
			{
				file.createNewFile();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		else
			file.delete();
	}
}
