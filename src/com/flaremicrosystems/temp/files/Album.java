package com.flaremicrosystems.temp.files;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.TreeSet;

import javax.imageio.ImageIO;

public class Album implements Comparable<Album>{
	
	static String[] image = {"png", "bmp", "jpg", "jpeg" ,"gif"};
	static String[] music = {"mp3", "wav"};
	
	public final String albumTitle;
	public final File albumFile;
	public final BufferedImage albumArt;
	public final TreeSet<Song> songs;
	
	public Album(File albumFile, BufferedImage albumArt, TreeSet<Song> songs, String title) {
		if(title == null)
			albumTitle = albumFile.getName();
		else albumTitle = title;
		this.albumFile = albumFile;
		this.albumArt = albumArt;
		this.songs = songs;
	}

	public static Album fromDir(File album, String title) {
		TreeSet<Song> songs = new TreeSet<Song>();
		BufferedImage albumArt = null;
		if(!album.isDirectory())
			return null;
		for(File file : album.listFiles())
		{
			String ext = file.toString().substring(file.toString().lastIndexOf(".")+1).trim();
			if(isOfType(ext, music))
			{
				Song song = new Song(file);
				songs.add(song);
			}
			else if(albumArt == null && isOfType(ext, image))
			{
				try {
					albumArt = ImageIO.read(file);
				} catch (IOException e) {
					albumArt = null;
					e.printStackTrace();
				}
			}
		}
		
		return new Album(album, albumArt, songs, title);
	}

	private static boolean isOfType(String ext, String[] type) {
		for(String s : type)
		{
			if(s.trim().equalsIgnoreCase(ext.trim()))
				return true;
		}
		return false;
	}

	public int compareTo(Album o) {
		return this.albumTitle.compareTo(o.albumTitle);
	}

	public static Album fromDir(File file) {
		return fromDir(file, null);
	}
	
}
