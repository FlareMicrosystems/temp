package com.flaremicrosystems.temp.ui;

import javax.swing.JPanel;
import javax.swing.JToggleButton;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.SwingConstants;

import com.flaremicrosystems.temp.files.Song;

import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.GridLayout;

public class PlaylistEntry extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the panel.
	 * @param mainFrame 
	 */
	
	MainFrame mainFrame;
	int index;
	Song song;
	JButton btnRemove = new JButton("X");
	JToggleButton btnItem;
	
	public PlaylistEntry(final Song song, int index, final MainFrame mainFrame) {
		this.index = index;
		setBorder(new EmptyBorder(10, 10, 10, 10));
		setLayout(new BorderLayout(0, 0));
		setOpaque(false);
		this.mainFrame = mainFrame;
		this.song = song;
		
		btnItem = new JToggleButton(song.title);
		btnItem.setPreferredSize(new Dimension(-1, -1));
		add(btnItem, BorderLayout.CENTER);
		btnItem.setHorizontalAlignment(SwingConstants.LEFT);
		btnItem.addActionListener(this);
		
		JPanel panel = new JPanel();
		panel.setOpaque(false);
		panel.setBorder(new EmptyBorder(0, 10, 0, 0));
		add(panel, BorderLayout.EAST);
		panel.setLayout(new GridLayout(1, 0, 10, 0));

		btnRemove.addActionListener(this);
		btnItem.addActionListener(this);
		panel.add(btnRemove);
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnRemove)
		{
			mainFrame.removeSong(index);
		}
		else if(e.getSource() == btnItem) 
		{
			mainFrame.paused = false;
			mainFrame.play(index);
		}
	}

	public void setSelected(boolean b) {
		btnItem.setSelected(b);
		btnItem.setBackground(b ? new Color(255, 253, 186) : null);
	}
}
